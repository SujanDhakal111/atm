package com.progressoft.induction.atm;
import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import com.progressoft.induction.atm.interfaces.ATM;
import com.progressoft.induction.atm.interfaces.BankingSystem;
import com.progressoft.induction.atm.interfaces.implementations.AtmImpl;
import com.progressoft.induction.atm.interfaces.implementations.BankingSystemImpl;
import com.progressoft.induction.atm.model.AccountDetails;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MainApplication {
    public static void main(String[] args) {
        ATM atm = new AtmImpl();
        BankingSystem bankingSystem = new BankingSystemImpl();
        AccountDetails accountDetails = new AccountDetails();
        Scanner accountScanner = new Scanner(System.in);

        accountDetails.setAccountInfo(getAccountInfo());

        System.out.println("Please enter your account number");
        String accountNo = accountScanner.next();


        if (accountDetails.getAccountInfo().containsKey(accountNo)) {

            BigDecimal accountBalance = bankingSystem.getAccountBalance(accountNo);
            System.out.println(accountBalance);

            System.out.println("Please enter amount to withdraw");
            BigDecimal withdrawAmount = accountScanner.nextBigDecimal();

            BigDecimal availableFund = new BigDecimal("5000.0");
            if (withdrawAmount.compareTo(availableFund) == -1) {

                if (withdrawAmount.compareTo(accountBalance) == -1) {
                    atm.withdraw(accountNo, withdrawAmount);
                    bankingSystem.debitAccount(accountNo, withdrawAmount);
                    System.out.println("successfully withdrawn Rs. " + withdrawAmount);
                    System.out.println("Now your remaining balance is" + bankingSystem.getAccountBalance(accountNo));
                } else {
                    throw new InsufficientFundsException("Insufficient Fund on your account");
                }

            } else {
                throw new NotEnoughMoneyInATMException("There is not enough money available in atm");
            }

        } else {
            throw new AccountNotFoundException("Account not found");
        }


    }


    protected static Map<String , BigDecimal> getAccountInfo(){
        Map<String , BigDecimal> accountInfo  = new HashMap<>();
        accountInfo.put("11111111", new BigDecimal("1000.0"));
        accountInfo.put("123456789", new BigDecimal("1000.0"));
        accountInfo.put("22222222", new BigDecimal("1000.0"));
        accountInfo.put("33333333", new BigDecimal("1000.0"));
        accountInfo.put("44444444", new BigDecimal("1000.0"));
        return  accountInfo;
    }
}
