package com.progressoft.induction.atm.interfaces.implementations;

import com.progressoft.induction.atm.Banknote;
import com.progressoft.induction.atm.interfaces.ATM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AtmImpl implements ATM {
    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal a) {
        double amount = a.doubleValue();
        List<Banknote> requiredNotes = new ArrayList<>();
        List<Banknote> notes= Arrays.asList(Banknote.FIVE_HUNDRED_JOD,Banknote.HUNDRED_JOD,Banknote.FIFTY_JOD,Banknote.TWENTY_JOD,Banknote.TEN_JOD,Banknote.FIVE_JOD);
        Banknote lastNote = notes.get(notes.size()-1 );
        while (amount != lastNote.getValue().doubleValue()){
            for (Banknote currentNote : notes){
                if(amount/1.5 >= currentNote.getValue().doubleValue()){
                    requiredNotes.add(currentNote);
                    amount = amount - currentNote.getValue().doubleValue();
                    break;
                }
            }
            if(amount == 0){
                break;
            }
        }
        while (amount == lastNote.getValue().doubleValue()){
            requiredNotes.add(lastNote);
            break;
        }
        System.out.println(requiredNotes);
        return requiredNotes;
    }
}
