package com.progressoft.induction.atm.interfaces.implementations;

import com.progressoft.induction.atm.interfaces.BankingSystem;
import com.progressoft.induction.atm.model.AccountDetails;

import java.math.BigDecimal;
import java.util.Map;

public class BankingSystemImpl implements BankingSystem {
    AccountDetails accountDetails = new AccountDetails();
    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        BigDecimal balance = accountDetails.getAccountInfo().get(accountNumber);

        return balance;
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        Map<String , BigDecimal> account = accountDetails.getAccountInfo();
        BigDecimal prevValue = account.get(accountNumber);
            account.replace(accountNumber , prevValue.subtract(amount));
       accountDetails.setAccountInfo(account);
    }
}
