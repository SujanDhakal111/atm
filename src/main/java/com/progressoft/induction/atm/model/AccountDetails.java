package com.progressoft.induction.atm.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class AccountDetails {
     private static Map<String, BigDecimal> accountInfo = new HashMap<>();


    public AccountDetails() {
    }

    public Map<String, BigDecimal> getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(Map<String , BigDecimal> accountInfo) {
        AccountDetails.accountInfo = accountInfo;
    }
}
